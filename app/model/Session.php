<?php

class Session{
    public $idsession;
    public $name;
    public $numberallowed;
    public $event;
    public $startdate;
    public $enddate;


    public function __construct() {
        DB::connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    }

    public function add($data) {

    }

    public function delete($data) {

    }

    public function getById($idsession) {
        return DB::queryOne('SELECT * FROM session WHERE idsession = :idsession', ['idsession' => $idsession], 'Session');
    }

    function getAll() {
        return DB::queryAll('SELECT * FROM session ORDER BY idsession ASC', null, 'Session');
    }
}