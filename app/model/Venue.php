<?php

class Venue{

    public $idvenue;
    public $name;
    public $capacity;

    public function __construct() {
        DB::connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    }

    public function add($data) {

    }

    public function delete($data) {

    }

    public function getById($idvenue) {
        return DB::queryOne('SELECT * FROM venue WHERE idvenue = :idvenue', ['idvenue' => $idvenue], 'Venue');
    }

    function getAll() {
        return DB::queryAll('SELECT * FROM venue ORDER BY idvenue ASC', null, 'Venue');
    }
}