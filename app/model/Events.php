<?php

class Events {

    public $idevent;
    public $name;
    public $datestart;
    public $dateend;
    public $numberallowed;
    public $venue;

    public function __construct() {
        DB::connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    }

    public function add($data) {
        
    }

    public function getById($idevent) {
        return DB::queryOne('SELECT * FROM event WHERE idevent = :idevent', ['idevent' => $idevent], 'Events');
    }

    function getAll() {
        return DB::queryAll('SELECT * FROM event ORDER BY idevent ASC', null, 'Events');
    }

    public function delete($data) {
        
    }

}