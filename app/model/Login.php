<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

class Login {
    public $id;
    public $name;
    public $password;
    public $role;
    
    
    public function __construct() {
        DB::connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    }
    
    function checkUserAndPassword($username, $password) {
        return DB::queryAll('SELECT * FROM attendee WHERE attendee.name = :name AND attendee.password = :password', ['name' => $username, 'password' => $password], 'Login');
    }
}