<?php

class Role{
    public $idrole;
    public $name;


    public function __construct() {
        DB::connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    }

    public function add($data) {

    }

    public function delete($data) {

    }

    public function getById($idrole) {
        return DB::queryOne('SELECT * FROM role WHERE idrole = :idrole', ['idrole' => $idrole], 'Role');
    }

    function getAll() {
        return DB::queryAll('SELECT * FROM role ORDER BY idrole ASC', null, 'Role');
    }
}