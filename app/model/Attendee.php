<?php

class Attendee {
    
    public $idattendee;
    public $name;
    public $password;
    public $role;

    public function __construct() {
        DB::connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    }

    public function add($data) {

    }

    public function getByID($idattendee) {
        return DB::queryOne('SELECT * FROM attendee WHERE idattendee = :idattendee', ['idattendee' => $idattendee], 'Attendee');
    }

    function getAll() {
        return DB::queryALL('SELECT * FROM attendee ORDER BY idattendee ASC', null, 'Attendee');
    }

    public function delete($data) {
        
    }

}