-- -----------------------------------------------------
-- Table `venue`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `venue` ;

CREATE TABLE IF NOT EXISTS `venue` (
  `idvenue` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `capacity` INT NULL,
  PRIMARY KEY (`idvenue`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `event`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `event` ;

CREATE TABLE IF NOT EXISTS `event` (
  `idevent` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `datestart` DATETIME NOT NULL,
  `dateend` DATETIME NOT NULL,
  `numberallowed` INT NOT NULL,
  `venue` INT NOT NULL,
  PRIMARY KEY (`idevent`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  INDEX `venue_fk_idx` (`venue` ASC))
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `session`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `session` ;

CREATE TABLE IF NOT EXISTS `session` (
  `idsession` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `numberallowed` INT NOT NULL,
  `event` INT NOT NULL,
  `startdate` DATETIME NOT NULL,
  `enddate` DATETIME NOT NULL,
  PRIMARY KEY (`idsession`))
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `attendee`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `attendee` ;

CREATE TABLE IF NOT EXISTS `attendee` (
  `idattendee` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `role` INT NULL,
  PRIMARY KEY (`idattendee`),
  INDEX `role_idx` (`role` ASC))
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `manager_event`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `manager_event` ;

CREATE TABLE IF NOT EXISTS `manager_event` (
  `event` INT NOT NULL,
  `manager` INT NOT NULL,
	PRIMARY KEY (`event`, `manager`))
ENGINE = MyISAM;

-- -----------------------------------------------------
-- Table `attendee_event`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `attendee_event` ;

CREATE TABLE IF NOT EXISTS `attendee_event` (
  `event` INT NOT NULL,
  `attendee` INT NOT NULL,
  `paid` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`event`, `attendee`))
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `attendee_session`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `attendee_session` ;

CREATE TABLE IF NOT EXISTS `attendee_session` (
  `session` INT NOT NULL,
  `attendee` INT NOT NULL,
  PRIMARY KEY (`session`, `attendee`))
ENGINE = MyISAM;

-- -----------------------------------------------------
-- Table `role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `roles` ;
DROP TABLE IF EXISTS `role` ;

CREATE TABLE IF NOT EXISTS `role` (
  `idrole` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idrole`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = MyISAM;

INSERT INTO `role` (`name`) values ('admin'),('event manager'),('attendee');

-- -----------------------------------------------------
-- Populating Table: venue
-- -----------------------------------------------------
INSERT INTO venue (name, capacity) values ('Mali Medo',150), ('Bar 45', 75);

-- -----------------------------------------------------
-- Populating Table: event
-- -----------------------------------------------------
INSERT INTO event (name, datestart, dateend, numberallowed, venue) VALUES ('EU Cup 22', '2022-06-24 20:00:00', '2022-06-30 23:59:59', 150, 1), ('Karaoke', '2021-11-07 19:30:00', '2021-11-08 23:00:00', 75, 2);

-- -----------------------------------------------------
-- Populating Table: session
-- -----------------------------------------------------
INSERT INTO session (name, numberallowed, event, startdate, enddate) VALUES ('ITA - FRA', 150, 1, '2022-06-24 20:00:00', '2022-06-24 22:00:00'), ('CRO - ENG', 150, 1, '2022-06-29 19:00:00', '2022-06-29 21:00:00'), ('80s', 75, 2, '2021-11-07 20:00:00', '2021-11-08 04:00:00'), ('90s', 75, 2, '2021-11-08 18:30:00', '2021-11-08 23:00:00');

-- -----------------------------------------------------
-- Populating Table: attendee
-- -----------------------------------------------------

INSERT INTO attendee (name, password, role) VALUES ('Lucija F.', 'plzgivepoints', 1), ('Mislav R.', 'KMbest', 1), ('K. Marasovic', 'ISTEServer', 2), ('B. Mihaljevic', 'ISTE330', 2), ('Sven Slivar', 'anlaki', 3), ('Domi', 'caniaskyouaquestion', 3);

-- -----------------------------------------------------
-- Populating Table: manager_event, attendee_event, and attendee_session
-- -----------------------------------------------------

INSERT INTO manager_event(event, manager) VALUES (1, 3), (2, 4);
INSERT INTO attendee_event(event, attendee, paid) VALUES (1, 5, 1), (2, 6, 1);
INSERT INTO attendee_session(session, attendee) VALUES (1, 3), (1, 5), (2, 3), (2, 5), (3, 4), (3, 6), (4, 4), (4, 6);